const exec = require("child_process").exec;

/**
 * git rev-parse --show-toplevel
 *
 * Shows path where .git resides
 *
 * Output:
 * ```
 * C:/Users/x/OneDrive/Documents/Websites/x
 * ```
 */
function getBaseDir(): Promise<string> {
  return new Promise((resolve, reject) => {
    exec("git rev-parse --show-toplevel", (err, stdout) => {
      if (err) {
        reject("Error acquiring the file path of the git repository");
      } else {
        resolve(stdout);
      }
    });
  });
}

/**
 * git rev-parse --abbrev-ref HEAD
 *
 * Outputs the name of the branch
 *
 * Output:
 * ```
 * master
 * ```
 */
function getBranch(): Promise<string> {
  return new Promise((resolve, reject) => {
    exec("git rev-parse --abbrev-ref HEAD", (err, stdout) => {
      if (err) {
        reject("Error getting the branch name");
      } else {
        resolve(stdout);
      }
    });
  });
}

/**
 * git ls-tree -r -l --full-tree master
 *
 * Shows all files
 *
 * Output:
 * ```
 * 100644 blob 64064e142e223e3b195fd476ab2145d676603aa6     391    scripts/prebuild/src/environments/environment.prod.ts
 * 100644 blob ca99a2b37f3b4bff902a0b6fa6d4789c6149a894    3082    scripts/prebuild/src/environments/environment.shared.ts
 * 100644 blob 3701701cff77090a6242be4a03f08dafbd7d722c     392    scripts/prebuild/src/environments/environment.ts
 * 100644 blob 3d293d5941a533562847aff0e9b01164378a9325    3351    scripts/prebuild/src/meta-default.constant.ts
 * 100644 blob 8b85fb5f9eeccbed3cbcbc755db9392f05bd1380   21225    scripts/prebuild/src/pre-build.ts
 * 100644 blob aa99ae42792b7b2c90b226d7ab184f467556ec51    2351    scripts/prebuild/tsconfig.json
 * 100644 blob fefa9d04f27f5880bae3e1d4672bfa8d18521c93    8673    symlink.sh
 * 100644 blob c01c13a7a05f9e115684ca9dbf1030a29119d3b6     743    tsconfig.json
 * 100644 blob e21f3479438198c812d95253eff1b60598029abd    3701    tslint.json
 * 100644 blob fcf8a79a9e101ba95bfcd0e4a60fd9fd42c91025      65    webpack.config.js
 * ```
 */
function getFileNames(branch: string): Promise<string> {
  return new Promise((resolve, reject) => {
    exec(
      "git ls-tree -r -l --full-tree --name-only " + branch,
      (err, stdout) => {
        if (err) {
          reject("Error getting the file names");
        } else {
          resolve(stdout);
        }
      }
    );
  });
}

export async function getFiles() {
  const [branch, baseDir] = await Promise.all([getBranch(), getBaseDir()]);
  const fileNames = await getFileNames(branch);
  return fileNames;
}
