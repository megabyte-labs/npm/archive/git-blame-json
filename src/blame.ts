// https://stackoverflow.com/questions/49370690/what-is-sourceline-resultline-in-git-blame-incremental-output
/**
 * git blame --incremental <file_path>
 *
 * Returns git blame data
 *
 * Output:
 * ```
 * 0000000000000000000000000000000000000000 1 1 2
 * author Not Committed Yet
 * author-mail <not.committed.yet>
 * author-time 1579435666
 * author-tz -0500
 * committer Not Committed Yet
 * committer-mail <not.committed.yet>
 * committer-time 1579435666
 * committer-tz -0500
 * summary Version of src/files.ts from src/files.ts
 * previous 58e14b8650f820181de8ad44e2958b3793d85171 src/files.ts
 * filename src/files.ts
 * 0000000000000000000000000000000000000000 5 5 1
 * previous 58e14b8650f820181de8ad44e2958b3793d85171 src/files.ts
 * filename src/files.ts
 * 0000000000000000000000000000000000000000 7 7 5
 * previous 58e14b8650f820181de8ad44e2958b3793d85171 src/files.ts
 * filename src/files.ts
 * 0000000000000000000000000000000000000000 13 13 11
 * previous 58e14b8650f820181de8ad44e2958b3793d85171 src/files.ts
 * filename src/files.ts
 * 0000000000000000000000000000000000000000 25 25 23
 * previous 58e14b8650f820181de8ad44e2958b3793d85171 src/files.ts
 * filename src/files.ts
 * 0000000000000000000000000000000000000000 49 49 1
 * previous 58e14b8650f820181de8ad44e2958b3793d85171 src/files.ts
 * filename src/files.ts
 * 0000000000000000000000000000000000000000 51 51 1
 * previous 58e14b8650f820181de8ad44e2958b3793d85171 src/files.ts
 * filename src/files.ts
 * 0000000000000000000000000000000000000000 53 53 1
 * previous 58e14b8650f820181de8ad44e2958b3793d85171 src/files.ts
 * filename src/files.ts
 * 0000000000000000000000000000000000000000 64 64 22
 * previous 58e14b8650f820181de8ad44e2958b3793d85171 src/files.ts
 * filename src/files.ts
 * 9c1120159b195d95697414ec702184c791cfbf1f 1 3 2
 * author Brian Zalewski
 * author-mail <brian@enviedsolutions.com>
 * author-time 1579432985
 * author-tz -0500
 * committer Brian Zalewski
 * committer-mail <brian@enviedsolutions.com>
 * committer-time 1579432985
 * committer-tz -0500
 * summary Base files and some notes.
 * filename src/files.ts
 * 9c1120159b195d95697414ec702184c791cfbf1f 4 6 1
 * filename src/files.ts
 * 9c1120159b195d95697414ec702184c791cfbf1f 7 12 1
 * filename src/files.ts
 * 9c1120159b195d95697414ec702184c791cfbf1f 8 24 1
 * filename src/files.ts
 * 9c1120159b195d95697414ec702184c791cfbf1f 10 48 1
 * filename src/files.ts
 * 9c1120159b195d95697414ec702184c791cfbf1f 12 50 1
 * filename src/files.ts
 * 9c1120159b195d95697414ec702184c791cfbf1f 14 52 1
 * filename src/files.ts
 * 9c1120159b195d95697414ec702184c791cfbf1f 16 54 10
 * filename src/files.ts
 * ```
 */
function getFileBlame(file: string): {} {

}