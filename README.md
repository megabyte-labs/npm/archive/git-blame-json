# Git Blame JSON
Git Blame JSON uses git and Node to find the commit ID, author, and date of whoever changed each line of code last. It does this by running the command "git blame" on every file in a git repository (or a subsection of your choosing). It outputs all the data into a single JSON file.

In order for this to work, you have to have git installed. Anywhere in a repository, you can run:
```
npm install -g gitblamejson
gitblamejson
```
The file named gitblame.json will be output to your working directory.

You can filter out a specific part of the repository and customize the location/name of the ouput file like this:
```
gitblamejson --input ./src --output ./path/to/newnamegitblame.json
```

If you need more customization, you can use the API.

## API Details
