var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const exec = require("child_process").exec;
/**
 * git rev-parse --show-toplevel
 *
 * Shows path where .git resides
 *
 * Output:
 * ```
 * C:/Users/x/OneDrive/Documents/Websites/x
 * ```
 */
function getBaseDir() {
    return new Promise((resolve, reject) => {
        exec("git rev-parse --show-toplevel", (err, stdout) => {
            if (err) {
                reject("Error acquiring the file path of the git repository");
            }
            else {
                resolve(stdout);
            }
        });
    });
}
/**
 * git rev-parse --abbrev-ref HEAD
 *
 * Outputs the name of the branch
 *
 * Output:
 * ```
 * master
 * ```
 */
function getBranch() {
    return new Promise((resolve, reject) => {
        exec("git rev-parse --abbrev-ref HEAD", (err, stdout) => {
            if (err) {
                reject("Error getting the branch name");
            }
            else {
                resolve(stdout);
            }
        });
    });
}
/**
 * git ls-tree -r -l --full-tree master
 *
 * Shows all files
 *
 * Output:
 * ```
 * 100644 blob 64064e142e223e3b195fd476ab2145d676603aa6     391    scripts/prebuild/src/environments/environment.prod.ts
 * 100644 blob ca99a2b37f3b4bff902a0b6fa6d4789c6149a894    3082    scripts/prebuild/src/environments/environment.shared.ts
 * 100644 blob 3701701cff77090a6242be4a03f08dafbd7d722c     392    scripts/prebuild/src/environments/environment.ts
 * 100644 blob 3d293d5941a533562847aff0e9b01164378a9325    3351    scripts/prebuild/src/meta-default.constant.ts
 * 100644 blob 8b85fb5f9eeccbed3cbcbc755db9392f05bd1380   21225    scripts/prebuild/src/pre-build.ts
 * 100644 blob aa99ae42792b7b2c90b226d7ab184f467556ec51    2351    scripts/prebuild/tsconfig.json
 * 100644 blob fefa9d04f27f5880bae3e1d4672bfa8d18521c93    8673    symlink.sh
 * 100644 blob c01c13a7a05f9e115684ca9dbf1030a29119d3b6     743    tsconfig.json
 * 100644 blob e21f3479438198c812d95253eff1b60598029abd    3701    tslint.json
 * 100644 blob fcf8a79a9e101ba95bfcd0e4a60fd9fd42c91025      65    webpack.config.js
 * ```
 */
function getFileNames(branch) {
    return new Promise((resolve, reject) => {
        exec("git ls-tree -r -l --full-tree --name-only " + branch, (err, stdout) => {
            if (err) {
                reject("Error getting the file names");
            }
            else {
                resolve(stdout);
            }
        });
    });
}
function run() {
    return __awaiter(this, void 0, void 0, function* () {
        const [branch, baseDir] = yield Promise.all([getBranch(), getBaseDir()]);
        const fileNames = yield getFileNames(branch);
        console.log(fileNames.split(/\r\n|\r|\n/g).filter(x => !!x));
    });
}
run();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmlsZXMuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi9zcmMvZmlsZXMudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7QUFBQSxNQUFNLElBQUksR0FBRyxPQUFPLENBQUMsZUFBZSxDQUFDLENBQUMsSUFBSSxDQUFDO0FBRTNDOzs7Ozs7Ozs7R0FTRztBQUNILFNBQVMsVUFBVTtJQUNqQixPQUFPLElBQUksT0FBTyxDQUFDLENBQUMsT0FBTyxFQUFFLE1BQU0sRUFBRSxFQUFFO1FBQ3JDLElBQUksQ0FBQywrQkFBK0IsRUFBRSxDQUFDLEdBQUcsRUFBRSxNQUFNLEVBQUUsRUFBRTtZQUNwRCxJQUFJLEdBQUcsRUFBRTtnQkFDUCxNQUFNLENBQUMscURBQXFELENBQUMsQ0FBQzthQUMvRDtpQkFBTTtnQkFDTCxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUM7YUFDakI7UUFDSCxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUMsQ0FBQyxDQUFDO0FBQ0wsQ0FBQztBQUVEOzs7Ozs7Ozs7R0FTRztBQUNILFNBQVMsU0FBUztJQUNoQixPQUFPLElBQUksT0FBTyxDQUFDLENBQUMsT0FBTyxFQUFFLE1BQU0sRUFBRSxFQUFFO1FBQ3JDLElBQUksQ0FBQyxpQ0FBaUMsRUFBRSxDQUFDLEdBQUcsRUFBRSxNQUFNLEVBQUUsRUFBRTtZQUN0RCxJQUFJLEdBQUcsRUFBRTtnQkFDUCxNQUFNLENBQUMsK0JBQStCLENBQUMsQ0FBQzthQUN6QztpQkFBTTtnQkFDTCxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUM7YUFDakI7UUFDSCxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUMsQ0FBQyxDQUFDO0FBQ0wsQ0FBQztBQUVEOzs7Ozs7Ozs7Ozs7Ozs7Ozs7R0FrQkc7QUFDSCxTQUFTLFlBQVksQ0FBQyxNQUFjO0lBQ2xDLE9BQU8sSUFBSSxPQUFPLENBQUMsQ0FBQyxPQUFPLEVBQUUsTUFBTSxFQUFFLEVBQUU7UUFDckMsSUFBSSxDQUNGLDRDQUE0QyxHQUFHLE1BQU0sRUFDckQsQ0FBQyxHQUFHLEVBQUUsTUFBTSxFQUFFLEVBQUU7WUFDZCxJQUFJLEdBQUcsRUFBRTtnQkFDUCxNQUFNLENBQUMsOEJBQThCLENBQUMsQ0FBQzthQUN4QztpQkFBTTtnQkFDTCxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUM7YUFDakI7UUFDSCxDQUFDLENBQ0YsQ0FBQztJQUNKLENBQUMsQ0FBQyxDQUFDO0FBQ0wsQ0FBQztBQUVELFNBQWUsR0FBRzs7UUFDaEIsTUFBTSxDQUFDLE1BQU0sRUFBRSxPQUFPLENBQUMsR0FBRyxNQUFNLE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQyxTQUFTLEVBQUUsRUFBRSxVQUFVLEVBQUUsQ0FBQyxDQUFDLENBQUM7UUFDekUsTUFBTSxTQUFTLEdBQUcsTUFBTSxZQUFZLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDN0MsT0FBTyxDQUFDLEdBQUcsQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLGFBQWEsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQy9ELENBQUM7Q0FBQTtBQUVELEdBQUcsRUFBRSxDQUFDIn0=